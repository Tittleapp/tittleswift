//
//  Util.swift
//  Tittle
//
//  Created by Thuan on 10/10/18.
//  Copyright © 2018 BlueCube. All rights reserved.
//

import Foundation
import UIKit

class Util {
    
}

//MARK: AppDelegate instance

let appDelegate = UIApplication.shared.delegate as! AppDelegate

let windowAppDelegate = appDelegate.window!

//MARK: App version

let appName = Bundle.main.infoDictionary?["CFBundleDisplayName"] as! String
let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
let buildVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as! String
let bundleIdentifier = Bundle.main.infoDictionary?["CFBundleIdentifier"] as! String

//MARK: Color from hex utility

func colorFromHex(_ hexString: String) -> UIColor? {
    let color = UIColor(hexString: hexString)
    
    return color
}

func sourceSansProRegular(size: CGFloat) -> UIFont? {
    let font = UIFont(name: R.font.sourceSansProRegular.fontName, size: size)
    return font
}

func sourceSansProItalic(size: CGFloat) -> UIFont? {
    let font = UIFont(name: R.font.sourceSansProIt.fontName, size: size)
    return font
}

func sourceSansProSemiBold(size: CGFloat) -> UIFont? {
    let font = UIFont(name: R.font.sourceSansProSemibold.fontName, size: size)
    return font
}
