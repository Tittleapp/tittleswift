//
//  FeatureModel.swift
//  Tittle
//
//  Created by Tuan Pham Hai  on 10/10/18.
//  Copyright © 2018 BlueCube. All rights reserved.
//

import Foundation

enum FeatureType: Int {
    case AppAccess
    case AppActivity
    case AppUsage
    case DataUsage
    case Schedule
    case Location
    case Geofencing
    case WebTracking
    case Tasks
    case Stories
    case Education
}

class FeatureModel: NSObject, NSCoding  {
    
    var name: String?
    var iconName: String?
    var index: Int?
    var type: FeatureType?
    override init() {
        super.init()
    }
    
    class func createFeature(index: Int) -> FeatureModel {
        let feature: FeatureModel = FeatureModel()
        feature.index = index
        feature.name = FeatureModel.featureName(index: index)
        feature.iconName = FeatureModel.featureName(index: index)
        return feature
    }
    
    func encode(with aCoder: NSCoder) {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
    }
    
    class func featureName(index: Int) -> String {
        let names = ["App Access", "App Activity", "App Usage", "Data Usage", "Schedule", "Location", "Geofencing", "Web Tracking", "Tasks", "Stories", "Education"]
        if index < names.count {
            return names[index]
        }
        return ""
    }
    
    class func iconName(index: Int) -> String {
        let iconNames: [String] = ["icon_app_access", "icon_app_activity", "icon_app_usage", "icon_app_data_usage", "icon_app_schedule", "icon_app_location", "icon_app_geofencing", "icon_app_webchecking", "icon_app_tasks", "icon_stories", "icon_education"]
        
        if index < iconNames.count {
            return iconNames[index]
        }
        return ""
    }
}
