//
//  BaseViewController.swift
//  Tittle
//
//  Created by Thuan on 10/10/18.
//  Copyright © 2018 BlueCube. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
}

extension BaseViewController {
    
    func setupView() {
        
    }
    
}
